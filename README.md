
![screenshot](https://raw.githubusercontent.com/sarveshspatil111/i3wm-nord/main/i3wm-nord.png)
![screenshot](https://raw.githubusercontent.com/sarveshspatil111/i3wm-nord/main/screenshots/SC1.png)
![screenshot](https://raw.githubusercontent.com/sarveshspatil111/i3wm-nord/main/screenshots/SC2.png)
![screenshot](https://raw.githubusercontent.com/sarveshspatil111/i3wm-nord/main/screenshots/SC3.jpg)

### Configurations
- [i3-gaps](https://github.com/sarveshspatil111/i3wm-nord/tree/main/i3) (Window Manager) + [i3status](https://github.com/sarveshspatil111/i3wm-nord/tree/main/i3status) (Status Bar)
- [Alacritty](https://github.com/sarveshspatil111/i3wm-nord/tree/main/alacritty) (Terminal)
- [OperatorMono Nerd Font](https://github.com/sarveshspatil111/i3wm-nord/tree/main/fonts) (Font)
- [NordArc Theme](https://github.com/sarveshspatil111/i3wm-nord/tree/main/themes) (Theme)
- [NordArc Icons](https://github.com/sarveshspatil111/i3wm-nord/tree/main/icons) (Icons)
- [Capitaine Cursors Light](https://github.com/sarveshspatil111/i3wm-nord/tree/main/icons) (Cursors)
- [NordTheme](https://www.nordtheme.com/) (Colorscheme)

### Wallpapers
- [Homescreen](https://raw.githubusercontent.com/sarveshspatil111/i3wm-nord/main/wallpapers/moon-nord.png)
- [Loginscreen](https://raw.githubusercontent.com/sarveshspatil111/i3wm-nord/main/wallpapers/tower-nord.png)
